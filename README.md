# Fresh Install
**Getting Up To Speed, In A Single Script, On Any Machine**

This series of BASH scripts controlled by a regular shell script, `main.sh`, gets you any version of:

* Eclipse IDE
* LunarG's Vulkan SDK
* more coming soon...

## How to Use The `main.sh` Script

### The Commands

* Global Verbosity (Can be overriden)
  * `--verbose 0`           : no output
  * `-v`   or `--verbose 1` : minimal output
  * `-vv`  or `--verbose 2` : play-by-play
  * `-vvv` or `--verbose 3` : detailed output
* Don't Use Any Recommended Versions
  * `-nr` or `--no-recommend`

### The Specifics

**Eclipse IDE**

* Clean Install and Clean Removal of Desired Version
  * `main.sh` : `line 82`
    * `eclipse_keywords="$eclipse_recommendation"`
  * For a menu of version options
    * `eclipse_keywords=""`
  * For a specific version
    * `eclipse_keywords="space separated keywords"`
* Launches from terminal or desktop

**LunarG's Vulkan SDK**

* Clean Install and Clean Removal of Desired Version
  * `main.sh` : `line 94`
    * `vulkan_sdk_keywords="$vulkan_sdk_recommendation"`
  * For a menu of version options
    * `vulkan_sdk_keywords=""`
  * For a specific version
    * `vulkan_sdk_keywords="space separated keywords"`


## Support
