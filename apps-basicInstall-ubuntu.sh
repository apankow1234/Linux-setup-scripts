# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# ---------------------- PREREQS ------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
apt update  -y
apt upgrade -y
apt install -y gdebi-core gdebi xfce4 xfce4-goodies
apt install -y git wget build-essential
# ---------------- RUNTIMES & CODECS --------------- #
# Java Runtime Environment
echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
add-apt-repository -y ppa:webupd8team/java
apt update  -y
apt install -y default-jdk openjdk-7-jdk \
    oracle-java8-installer \
    oracle-java8-set-default
apt install -y
# NVIDIA or AMD drivers
# add-apt-repository -y ppa:graphics-drivers/ppa
# apt     update  -y
## apt     install -y nvidia-...
# apt     install -y nvidia-387
# add-apt-repository -y ppa:oibaf/graphics-drivers
# apt update  -y
# apt     update  -y
# apt     upgrade -y
# echo "Section \"Device\""                    >> /etc/X11/xorg.conf
# echo "    Identifier \"AMDGPU\""             >> /etc/X11/xorg.conf
# echo "    Driver \"amdgpu\""                 >> /etc/X11/xorg.conf
# echo "    Option \"AccelMethod\" \"glamor\"" >> /etc/X11/xorg.conf
# echo "    Option \"DRI\" \"3\""              >> /etc/X11/xorg.conf
# echo "EndSection"                            >> /etc/X11/xorg.conf
# PlayOnLinux
apt install -y playonlinux
# FFMPEG
apt install -y ffmpeg
# -------------------------------------------------- #
apt-get install -f
apt update  -y
# --------------------- SERVICES ------------------- #
# DropBox
apt install -y nautilus-dropbox
# Google Drive
apt install -y gnome-online-accounts
# VNC
apt install -y tightvncserver

# -------------------------------------------------- #
apt-get install -f
apt update  -y
# -------------------- LANGUAGES ------------------- #
# GCC
apt install -y gcc
# G++
apt install -y g++
# Clang
apt install -y clang
# CMAKE
apt install -y cmake
git clone git://anongit.kde.org/extra-cmake-modules
cd extra-cmake-modules
mkdir build && cd build    
cmake ..
make && sudo make install
# Python 3.x
apt install -y python3-minimal python3-pip
# PERL
apt install -y perl
# GAWK
apt install -y gawk
# -------------------------------------------------- #
apt-get install -f
apt update  -y
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #



# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# ---------------------- COMMON -------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# VLC
apt install -y vlc vlc-plugin-access-extra
# Synergy KVM
apt install -y synergy
# TeamViewer
# Libre Office
add-apt-repository -y ppa:libreoffice/ppa
apt update  -y
apt upgrade -y
apt install -y libreoffice
# Chromium
apt install -y chromium-browser
# Mozilla Firefox
apt install -y firefox
# Dia
apt install -y dia
sed -i '/^dia %F/ s/.*/Exec=env LIBOVERLAY_SCROLLBAR=0 dia %F/'
# Zoom
apt install -y zoom
# -------------------------------------------------- #
apt-get install -f
apt update  -y
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #




# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# ------------------- DEVELOPMENT ------------------ #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------- COMP. SCI. ------------------ #
# EMACS
apt install -y emacs
# SQLite
apt install -y sqlite sqlitebrowser
# MongoDB
apt install -y mongodb
# Matlab
# -------------------------------------------------- #
# --------------------- GRAPHICS ------------------- #
# Eclipse
apt install -y eclipse
# Vulkan
# OpenGL
# GLFW
# SDL2
# PyVulkan
# PyGL
# Python GLFW
# PySDL2
# -------------------------------------------------- #
# ----------------------- WEB ---------------------- #
# Sublime
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
apt-add-repository -y "deb https://download.sublimetext.com/ apt/stable/"
apt install -y sublime-text
# Apache
apt install -y apache2
# MySQL
apt install -y mysql-server mysql-workbench
# PHP
apt install -y php-pear php-fpm php-dev php-zip php-curl \
    php-xmlrpc php-gd php-mysql php-mbstring php-xml libapache2-mod-php
# RUST
curl https://sh.rustup.rs -sSf | sh
# -------------------------------------------------- #
# -------------------- NEW MEDIA ------------------- #
# Visual Studio Code
apt install -y software-properties-common apt-transport-https
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | apt-key add -
add-apt-repository -y "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
apt install -y code
# Python 2.x
apt install -y python-minimal python3-pip
# Android Studio
snap install android-studio
# NodeJS
apt install -y nodejs npm
# -------------------------------------------------- #
apt-get install -f
apt update  -y
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #



# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# --------------------- DESIGN --------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# ------------------- ORGANIZATION ----------------- #
# DigiKam
apt install -y digikam
git clone https://github.com/KDE/kipi-plugins.git
apt install -y qt5-default libqt5*-dev libkf5kdelibs4support5-bin
export VERBOSE=1
export QTDIR=/usr/lib/qt5/
export PATH=$QTDIR/bin:$PATH
cmake .
make
make install
## Adobe Bridge/ Lightroom
# -------------------------------------------------- #
# --------------- IMAGES & PUBLISHING -------------- #
# GIMP
add-apt-repository -y ppa:otto-kesselgulasch/gimp
apt update  -y
apt install -y gimp
# Krita
add-apt-repository -y ppa:kritalime/ppa
apt update  -y
apt install -y krita
## Adobe Photoshop
# InkScape
add-apt-repository -y ppa:inkscape.dev/stable
apt update  -y
apt install -y inkscape
## Adobe Illustrator
# Scribus
add-apt-repository -y ppa:scribus/ppa
apt update  -y
apt install -y scribus
## Adobe InDesign
## Adobe XD
# -------------------------------------------------- #
# ------------------- 3D & GAMES ------------------- #
# Blender
add-apt-repository -y ppa:thomas-schiex/blender
apt update  -y
apt install -y blender
## Autodesk Maya
## SideFX Houdini
## Maxon Cinema4D
# LuxRender
## VRay
## Octane
## Arnold
# Steam
add-apt-repository -y multiverse
apt update  -y
apt install -y steam
# Unity
# Unreal
# Amazon's Lumberyard
# -------------------------------------------------- #
# ------------------ AUDIO & MUSIC ----------------- #
# MuseScore
apt install -y musescore
# Audacity
apt install -y audacity
## Adobe Audition
# Let's Make Music Software
apt install -y lmms
## Avid Pro Tools
## FL Studio (Fruity Loops)
# -------------------------------------------------- #
# ----------------- Video & MoGraph ---------------- #
# KdenLive
add-apt-repository -y ppa:kdenlive/kdenlive-stable
apt update  -y
apt upgrade -y
apt install -y kdenlive
# Open Broadcaster Software
add-apt-repository -y ppa:obsproject/obs-studio
apt update  -y
apt install -y obs-studio
# Natron
## Adobe After Effects
## Adobe Premier Pro
## The Foundry's NukeX
# -------------------------------------------------- #
apt-get install -f
apt update  -y
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #



# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# ----------------- HARDWARE DESGIN ---------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# ---------------- ARCHITECTURE & ID --------------- #
# FreeCAD
add-apt-repository -y ppa:freecad-maintainers/freecad-stable
apt update  -y
apt install -y freecad freecad-doc
apt upgrade -y
# -------------------------------------------------- #
# ---------------------- EECS ---------------------- #
# Logisim
apt install -y logisim
# fritzing
apt install -y fritzing fritzing-data fritzing-parts
# KiCAD
add-apt-repository -y ppa:js-reynaud/kicad-5
apt update  -y
apt install -y kicad
## Altera Quartus
## Xilinx Vivado
## Xilinx ISE WebPack
# -------------------------------------------------- #
apt-get install -f
apt update  -y
apt upgrade -y
apt autoremove -y
apt autoclean -y
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #
# -------------------------------------------------- #

# Ubuntu restricted extras
apt install -y ubuntu-restricted-extras
# Google Chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
gdebi google-chrome-stable_current_amd64.deb
# Google Drive Setup
gnome-control-center online-accounts



# restart -h now






