# /
# ├ Files/        ## user storage
# │ └ ...
# │
# ├ Apps/         ## applications' program files and exes folder
# │ ├ App1/
# │ │ ꜖ app1.run
# │ │
# │ └ App2/
# │   └ app2.run
# │ 
# └ usr/          ## applications' command line launchers folder
#   └ bin/
#     ├ app1      ## "/Apps/App1/app1.run"
#     │
#     └ app2      ## "/Apps/App2/app2.run"

createCommandLineLauncher()
{
   local exe_filepath=$1
   local exe_cmd=`echo $exe_filepath | grep -Po "([\w\d\_\-]+)"`
   local cmd_file="/usr/bin/$exe_cmd"
   touch $cmd_file
   echo "#! /bin/bash -f" >> $cmd_file
   echo "$exe_filepath"   >> $cmd_file
}