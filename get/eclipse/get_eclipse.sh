#!/bin/bash
#==================================================================#
# Fresh Install
# Getting Up To Speed, In A Single Script, On Any Machine
# 
# By: Andrew Pankow
# 
# 
# Install/Remove Eclipse IDE
# Choose your version...
# 
#------------------------------------------------------------------#
#==================================================================#
# Get locality and include required functionality
#------------------------------------------------------------------#
# Painfully manual method
curr=`pwd`
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ../../includes
# Get absolute filepath to the includes directory
includes_dir=`pwd`
# Include all of the basic functions required
source $includes_dir/basic_func.sh
# Last part of the painfully manual method
cd $curr
#------------------------------------------------------------------#






#==================================================================#
# Initialization of key variables
#------------------------------------------------------------------#
src="http://mirror.csclub.uwaterloo.ca/eclipse/technology/epp/downloads/release/"
file_ext="\.tar\.gz"

files_grep_regex="\=\"(.*?linux.*?$(uname -m).*?$file_ext\")"
files_sed_regex="s/\=\"//g;s/\"$//g"

dl_loc=""

# keywords deliminated by...
keywords=""
delim=" "
# 
remove_only=0
verbosity_lvl=0
#------------------------------------------------------------------#
#==================================================================#
# Main Interface
#------------------------------------------------------------------#
while [ $# -gt 0 ]; do
	case "$1" in
		-s|--source)
			shift
			if [ $# -gt 0 ]; then
				src=$1
#				echo "Source: $source"
			else
				echo "No source URL given."
				exit 1
			fi
			shift
			;;
		-dl|--download-location)
			shift
			if [ $# -gt 0 ]; then
				dl_loc=$1
#				echo "Download Location: $dl"
			else
				echo "No download location given."
				exit 1
			fi
			shift
			;;
		-d|--delim)
			shift
			if [ $# -gt 0 ]; then
				delim=$1
#				echo "Deliminator: $delim"
			else
				echo "No deliminator given."
				exit 1
			fi
			shift
			;;
		-k|--keywords)
			shift
			if [ $# -gt 0 ]; then
				keywords=$1
			else
				echo "No keywords given."
				exit 1
			fi
#			echo "Keywords: $keywords"
			shift
			;;
		-rm|--remove)
			remove_only=1
			shift
			;;
		--verbose)
			shift
			if [ $# -gt 0 ]; then
				verbosity_lvl=$1
				shift
			else
				verbosity_lvl=1
			fi
#			echo "Verbosity Level: $verbosity_lvl"
			;;
		-v)
			verbosity_lvl=1
			shift
			;;
		-vv)
			verbosity_lvl=2
			shift
			;;
		-vvv)
			verbosity_lvl=3
			shift
			;;
		*)
			break
			;;
	esac
done
#------------------------------------------------------------------#





#==================================================================#
# Give the download location a default if none are provided
#------------------------------------------------------------------#
dl_loc=$( DefaultDownloadLocation $dl_loc )
#------------------------------------------------------------------#
#==================================================================#
# Aquire the Eclipse from the web
#------------------------------------------------------------------#
GetEclipse()
{
	bash $includes_dir/dl/downloader.sh \
		-s $src \
		-n "Eclipse" \
		-fp "$files_grep_regex" \
		-mp "$files_sed_regex" \
		-e $file_ext \
		-k "$keywords" \
		-d "$delim" \
		-dl "$dl_loc" \
		--verbose $verbosity_lvl \
		-cln
}
#------------------------------------------------------------------#
#==================================================================#
# Uninstall and Remove Eclipse
#------------------------------------------------------------------#
RemoveApp()
{
	Verbose 2 $verbosity_lvl "Removing application files..."
	rm -fr /opt/eclipse*

	local curr=`pwd`
	local profile_file=".eclipse"
	local profiles=`find /home -name "$profile_file" -type d | sed -e "s/$profile_file//g"`
	for i in ${profiles[@]}; do
		Verbose 3 $verbosity_lvl "Removing profile in: $i"
		cd $i
		rm -r $profile_file
	done
	cd $curr

	Verbose 3 $verbosity_lvl "Uninstalling desktop launch abilities."
	local desktop="/usr/share/applications/eclipse.desktop"
	rm -f $desktop
	Verbose 3 $verbosity_lvl "Removing icon."
	local icon_img="/usr/share/pixmaps/eclipse.xpm"
	rm -f $icon_img
	Verbose 3 $verbosity_lvl "Removing terminal command."
	local cmd_file="/usr/bin/eclipse"
	rm -f $cmd_file

	Verbose 2 $verbosity_lvl "Previous versions of Eclipse have been removed."
}
#------------------------------------------------------------------#
#==================================================================#
# Make and install the desktop GUI launcher
#------------------------------------------------------------------#
MakeDesktop()
{
	#create desktop/menu icon
	local desktop="/usr/share/applications/eclipse.desktop"
	if [ ! -f $desktop ]; then
		touch $desktop
		chmod 777 $desktop
	else
		> $desktop
	fi
	echo "[Desktop Entry]"                            >> $desktop
	echo "Name=Eclipse"                               >> $desktop
	echo "Type=Application"                           >> $desktop
	echo "Exec=/opt/eclipse/eclipse"                  >> $desktop
	echo "Terminal=false"                             >> $desktop
	echo "Icon=/opt/eclipse/icon.xpm"                 >> $desktop
	echo "Comment=Integrated Development Environment" >> $desktop
	echo "NoDisplay=false"                            >> $desktop
	echo "Categories=Development;IDE;"                >> $desktop
	echo "Name[en]=Eclipse"                           >> $desktop
	echo "Name[en_US]=Eclipse"                        >> $desktop
	Verbose 2 $verbosity_lvl "Eclipse now has a desktop icon and can be launched from your desktop."
	if [ $verbosity_lvl -gt 2 ]; then
		cat $desktop
	fi
	desktop-file-install $desktop
	cp /opt/eclipse/icon.xpm /usr/share/pixmaps/eclipse.xpm
}
#------------------------------------------------------------------#
#==================================================================#
# Correct the colors in the menus to be readable
#------------------------------------------------------------------#
correct_menus()
{
#	selected_theme="Ambiance"
	selected_theme=$1
	# make menus readable
	local new_fg_color="#000000"
	local new_bg_color="#f5f5c5"
	local fg_color_ln="tooltip_fg_color:"
	local bg_color_ln="tooltip_bg_color:"
	local fg_color=$fg_color_ln$new_fg_color
	local fg_color=$bg_color_ln$new_bg_color
	local menu_dir="/usr/share/themes/$selected_theme"
	sed -i "s/$fg_color_ln\#\d{6}/$fg_color/" "$menu_dir/gtk-2.0/gtkrc"
	sed -i "s/$fg_color_ln\#\d{6}/$fg_color/" "$menu_dir/gtk-3.0/settings.ini"
	sed -i "s/$fg_color_ln\#\d{6}/$fg_color/" "$menu_dir/gtk-3.0/gtk-main.css"

}
#------------------------------------------------------------------#
#==================================================================#
# Extract and Install the Program
#------------------------------------------------------------------#
InstallApp()
{
	# Get downloaded Eclipse archive
	local dl_file=$( FindFile $dl_loc "$keywords" "$file_ext" )
	Verbose 3 $verbosity_lvl "File to Install: $dl_file"

	# Uninstall previous versions
	Verbose 2 $verbosity_lvl "Removing Previous versions of Eclipse..."
	RemoveApp

	Verbose 1 $verbosity_lvl "Installing Eclipse..."
	if [ $verbosity_lvl -gt 2 ]; then
		tar -xvz -f $dl_file -C /opt/
	else
		tar -xz -f $dl_file -C /opt/
	fi
	Verbose 2 $verbosity_lvl "Eclipse files have successfully been placed in /opt ."

	Verbose 2 $verbosity_lvl "Creating terminal command..."
	MakeCommand "/opt/eclipse/eclipse"
	Verbose 2 $verbosity_lvl "Creating desktop icon & launcher..."
	MakeDesktop

	Verbose 1 $verbosity_lvl "Eclipse has been installed."
}
#------------------------------------------------------------------#





#==================================================================#
# Main Functionality
#------------------------------------------------------------------#
if [ $remove_only -ne 0 ]; then
	RemoveApp
	Verbose 1 $verbosity_lvl "Eclipse has been removed."
else
	GetEclipse
	InstallApp
fi
#------------------------------------------------------------------#






