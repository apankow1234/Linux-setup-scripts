#!/bin/bash
#==================================================================#
# Fresh Install
# Getting Up To Speed, In A Single Script, On Any Machine
# 
# By: Andrew Pankow
# 
# 
# Install/Remove LunarG's Vulkan SDK
# 
#------------------------------------------------------------------#
#==================================================================#
# Get locality and include required functionality
#------------------------------------------------------------------#
# Painfully manual method
curr=`pwd`
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ../../includes
# Get absolute filepath to the includes directory
includes_dir=`pwd`
# Include all of the basic functions required
source $includes_dir/basic_func.sh
# Last part of the painfully manual method
cd $curr
#------------------------------------------------------------------#





#==================================================================#
# Initialization of key variables
#------------------------------------------------------------------#
src="https://vulkan.lunarg.com/sdk/home"
file_ext="\.run"

files_grep_regex="linux(.*?)$"
files_sed_regex="s/\s*linux:\s*\[\"//g;s/\"\]\,//g;s/\"\,\"/\n/g"
install_file_pattern="\"vulkansdk-linux-x86_64-\$option.run\""
full_url_pattern="\"https://sdk.lunarg.com/sdk/download/\$option/linux/\$install_file?Human=true\""

dl_loc=""
keywords=""
delim=" "
verbosity_lvl=0
remove_only=0
#------------------------------------------------------------------#
#==================================================================#
# Main Interface
#------------------------------------------------------------------#
while [ $# -gt 0 ]; do
	case "$1" in
		-s|--source)
			shift
			if [ $# -gt 0 ]; then
				src=$1
#				echo "Source: $source"
			else
				echo "No source URL given."
				exit 1
			fi
			shift
			;;
		-dl|--download-location)
			shift
			if [ $# -gt 0 ]; then
				dl_loc=$1
#				echo "Download Location: $dl"
			else
				echo "No download location given."
				exit 1
			fi
			shift
			;;
		-d|--delim)
			shift
			if [ $# -gt 0 ]; then
				delim=$1
#				echo "Deliminator: $delim"
			else
				echo "No deliminator given."
				exit 1
			fi
			shift
			;;
		-k|--keywords)
			shift
			if [ $# -gt 0 ]; then
				keywords=$1
			else
				echo "No keywords given."
				exit 1
			fi
#			echo "Keywords: $keywords"
			shift
			;;
		-rm|--remove)
			remove_only=1
			shift
			;;
		--verbose)
			shift
			if [ $# -gt 0 ]; then
				verbosity_lvl=$1
				shift
			else
				verbosity_lvl=1
			fi
#			echo "Verbosity Level: $verbosity_lvl"
			;;
		-v)
			verbosity_lvl=1
			shift
			;;
		-vv)
			verbosity_lvl=2
			shift
			;;
		-vvv)
			verbosity_lvl=3
			shift
			;;
		*)
			break
			;;
	esac
done
#------------------------------------------------------------------#





#==================================================================#
# Give the download location a default if none are provided
#------------------------------------------------------------------#
dl_loc=$( DefaultDownloadLocation $dl_loc )
#------------------------------------------------------------------#
#==================================================================#
# Aquire the Vulkan SDK from the web
#------------------------------------------------------------------#
GetVulkanSDK()
{
	bash $includes_dir/dl/downloader.sh \
		-s $src \
		-n "Vulkan_SDK" \
		-fp "$files_grep_regex" \
		-mp "$files_sed_regex" \
		-fu "$full_url_pattern" \
		-if "$install_file_pattern" \
		-e $file_ext \
		-k "$keywords" \
		-d " " \
		-e $file_ext \
		-dl "$dl_loc" \
		--verbose $verbosity_lvl \
		-cln
}
#------------------------------------------------------------------#
#==================================================================#
# Uninstall and Remove the SDK
#------------------------------------------------------------------#
RemoveSDK()
{
	Verbose 2 $verbosity_lvl "Removing application files..."
	local sdk_file="VulkanSDK"
	local sdk_files=`find /home -name "$sdk_file" -type d | sed -e "s/$sdk_file//g"`
	for i in ${sdk_files[@]}; do
		Verbose 3 $verbosity_lvl "Removing profile in: $i"
		cd $i
		rm -r $sdk_file
	done
	Verbose 2 $verbosity_lvl "All versions of Vulkan SDK have been removed."
}
#------------------------------------------------------------------#
#==================================================================#
# Extract and Install the SDK
#------------------------------------------------------------------#
InstallSDK()
{
	# Get downloaded Vulkan SDK script
	dl_file=$( FindFile $dl_loc "$keywords" "$file_ext" )
	Verbose 2 $verbosity_lvl "File to Install: $dl_file"

#	case $os_flavor in
#		"ubuntu")
#			sudo apt-get install -fy \
#				libglm-dev \
#				graphviz \
#				libxcb-dri3-0 \
#				libxcb-present0 \
#				libpciaccess0 \
#				cmake \
#				libpng-dev \
#				libxcb-keysyms1-dev \
#				libxcb-dri3-dev \
#				libx11-dev \
#				libmirclient-dev \
#				libwayland-dev \
#				libxrandr-dev
#
#			lts_version=16
#			if [ $mjr_version -lt $lts_version ]; then
#				echo "Older version of Ubuntu"
#				sudo apt-get install \
#					git \
#					libpython2.7
#			fi
#			;;
#
#		"fedora")
#			sudo dnf install -y \
#				glm-devel \
#				graphviz \
#				cmake \
#				libpng-devel \
#				wayland-devel \
#				libpciaccess-devel \
#				libX11-devel \
#				libXpresent \
#				libxcb \
#				xcb-util \
#				libxcb-devel \
#				libXrandr-devel \
#				xcb-util-keysyms-devel.x86_64
#			;;
#		*)
#			;;
#	esac

	local curr=`pwd`
	for i in $( ls /home ); do
		cd /home/$i
		Verbose 1 $verbosity_lvl "Installing Vulkan SDK..."
		Verbose 3 $verbosity_lvl "Install File: $dl_file"
		sh $dl_file
#		source VulkanSDK/$sdk_version/setup-env.sh
	done
	cd $curr

	Verbose 1 $verbosity_lvl "Vulkan SDK has been installed."
}
#------------------------------------------------------------------#





#==================================================================#
# Main Functionality
#------------------------------------------------------------------#
if [ $remove_only -ne 0 ]; then
	RemoveSDK
else
	GetVulkanSDK
	InstallSDK
fi
#------------------------------------------------------------------#

