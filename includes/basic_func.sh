#!/bin/bash
#==================================================================#
# Fresh Install
# Getting Up To Speed, In A Single Script, On Any Machine
# 
# By: Andrew Pankow
# 
# 
# Basic functionality used in this SDK
# 
#------------------------------------------------------------------#
#==================================================================#
# Assert that any script requiring this file has root authority
#------------------------------------------------------------------#
CheckRoot()
{
	if [ `whoami` != root ]; then
		echo "Please run this script using root."
		 exit
	fi
}
#Make sure running as root
CheckRoot
#------------------------------------------------------------------#





#==================================================================#
# Make sure there is a default download location set if none is
# given based on the flavor of Linux
#
# Argument:
#   $1 ---> Download Location
#------------------------------------------------------------------#
DefaultDownloadLocation()
{
	if [ -z "$1" ]; then
		if [ -f /etc/redhat-release ]; then # default for RedHat flavors
			echo "$HOME"
		elif [ -f /etc/debian_version ]; then # default for Debian flavors
			echo "$HOME/Downloads/"
		fi
	else
		echo $1
	fi
}
#------------------------------------------------------------------#
#==================================================================#
# Find a file based on keywords and/or a file extension
#
# Arguments:
#   $1 | $dl_loc   ---> Download location
#   $2 | $keywords ---> Space separated string
#   $3 | $file_ext ---> Special character escaped file extension
#------------------------------------------------------------------#
FindFile()
{
	local dl_loc="$1"
	local keywords="$2"
	local file_ext="$3"
	local findin="find $dl_loc "
	IFS=" " read -ra KWORDS <<< "$keywords"
	for kw in ${KWORDS[@]}; do
		findin+="-iname \"*$kw*\" "
	done;
	if [ ! -z "$file_ext" ]; then
		findin+="-name \"*$file_ext*\""
	fi
	echo `eval ${findin}`
}
#------------------------------------------------------------------#
#==================================================================#
# Print to the terminal based on desired levels of verbosity
#
# Arguments:
#   $1 | $this_lvl ---> The level of verbocity required to print
#   $2 | $v_lvl    ---> The verbocity level of script
#   $3 | $output   ---> The string to print if verbocity reached
#------------------------------------------------------------------#
Verbose()
{
	local this_lvl=$1
	local v_lvl=$2
	local output="$3"
	if [ $v_lvl -ge $this_lvl ]; then
		echo "$output"
	fi
}
#------------------------------------------------------------------#
#==================================================================#
# Empty or Create a file
#
# Argument:
#   $1 ---> Filepath to create or empty
#------------------------------------------------------------------#
FreshFile()
{
	if [ ! -f $1 ]; then
		touch $1
		chmod 777 $1
	else	
		> $1
	fi
}
#------------------------------------------------------------------#
#==================================================================#
# Extract the time from a string
#
# Argument:
#   $1 ---> String with [ HH:MM ] string in it
#------------------------------------------------------------------#
GetTime()
{
	echo "$( echo "$1" | grep -Po "[^\:](\d+):(\d+)" )"
}
#------------------------------------------------------------------#
#==================================================================#
# Get difference between two HH:MM times in Minutes
#
# Arguments:
#   $1 | $LATE  ---> Later time    [ HH:MM ]
#   $2 | $EARLY ---> Earlier time  [ HH:MM ]
#------------------------------------------------------------------#
TimeDiff()
{
	IFS=":" read -ra LATE  <<< $( GetTime "$1" )
	IFS=":" read -ra EARLY <<< $( GetTime "$2" )
	local sH=${EARLY[0]#0}
	local sM=${EARLY[1]#0}
	local eH=${LATE[0]#0}
	local eM=${LATE[1]#0}
	echo "$(( $(( $((10#$sH))*60+$((10#$sM)) ))-$(( $((10#$eH))*60+$((10#$eM)) )) ))"
}
#------------------------------------------------------------------#
#==================================================================#
# Get the number of Minutes since a file was modified last
#
# Argument:
#   $1 ---> Filepath
#------------------------------------------------------------------#
TimeSinceLastModify()
{
	echo $( TimeDiff `ls -lu $1` `date` )
}
#------------------------------------------------------------------#
#==================================================================#
# Make a terminal command to run an app
#
# Argument:
#   $1 | $exe_filepath ---> Executable's filepath
#------------------------------------------------------------------#
MakeCommand()
{
	local exe_filepath=$1
	local exe_cmd=`echo $exe_filepath | grep -Po "([\w\d\_\-]+$)"`
	local cmd_file="/usr/bin/$exe_cmd"
	FreshFile $cmd_file
	echo "#!/bin/bash"   >> $cmd_file
	echo "$exe_filepath" >> $cmd_file
}
#------------------------------------------------------------------#


