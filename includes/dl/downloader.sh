#!/bin/bash
#==================================================================#
# Fresh Install
# Getting Up To Speed, In A Single Script, On Any Machine
# 
# By: Andrew Pankow
# 
# 
# Main Downloader for this SDK
# 
#------------------------------------------------------------------#
#==================================================================#
# Get locality and include required functionality
#------------------------------------------------------------------#
# Painfully manual method
curr=`pwd`
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ../../includes
# Get absolute filepath to the includes directory
includes_dir=`pwd`
# Include all of the basic functions required
source $includes_dir/basic_func.sh
# Last part of the painfully manual method
cd $curr
#------------------------------------------------------------------#






#==================================================================#
# Initialization of key variables
#------------------------------------------------------------------#
menu_file="__menu__"
files_file="__files__"
all_file="__all_files__"
folders_grep_regex=">(\w+\b\/).*?(\d+\-\w+\-\d+)"
folders_sed_regex="s/\s\+/\t/g;s/<\/a>//g;s/^>//g;s/\///g;"
files_grep_regex="\=\"[_a-z0-9]*\""
files_sed_regex="//g"

# files' source address
source=""
# files' extension
ext=""
# local location
dl_loc=""

# keywords deliminated by...
keywords=""
delim=" "
# for the user's benefit only.
name=""
max_time=120
cln=0
verbosity_lvl=0

install_file=""
install_file_pattern=""
full_url=""
full_url_pattern=""
#------------------------------------------------------------------#
#==================================================================#
# Main Interface
#------------------------------------------------------------------#
while [ $# -gt 0 ]; do
	case "$1" in
		-n|--name)
			shift
			if [ $# -gt 0 ]; then
				name=$1
				menu_file="__"$name"_menu__"
				files_file="__"$name"_files__"
				all_file="__all_"$name"_files__"
#				echo "Name: $name"
			else
				echo "No name given."
				exit 1
			fi
			shift
			;;
		-d|--delim)
			shift
			if [ $# -gt 0 ]; then
				delim=$1
#				echo "Deliminator: $delim"
			else
				echo "No deliminator given."
				exit 1
			fi
			shift
			;;
		-s|--source)
			shift
			if [ $# -gt 0 ]; then
				source=$1
#				echo "Source: $source"
			else
				echo "No source URL given."
				exit 1
			fi
			shift
			;;
		-e|--ext)
			shift
			if [ $# -gt 0 ]; then
				ext=$1
#				echo "Extension: $ext"
			else
				echo "No extension given."
				exit 1
			fi
			shift
			;;
		-fp|--file-pattern)
			shift
			if [ $# -gt 0 ]; then
				files_grep_regex=$1
#				echo "Pattern: $file_grep_regex"
			else
				echo "No pattern given."
				exit 1
			fi
			shift
			;;
		-mp|--menu-pattern)
			shift
			if [ $# -gt 0 ]; then
				files_sed_regex=$1
#				echo "Pattern: $file_sed_regex"
			else
				echo "No pattern given."
				exit 1
			fi
			shift
			;;
		-fu|--full_url-pattern)
			shift
			if [ $# -gt 0 ]; then
				full_url_pattern=$1
#				echo "Pattern: $full_url_pattern"
			else
				echo "No pattern given."
				exit 1
			fi
			shift
			;;
		-if|--install-file-pattern)
			shift
			if [ $# -gt 0 ]; then
				install_file_pattern=$1
#				echo "Pattern: $install_file_pattern"
			else
				echo "No pattern given."
				exit 1
			fi
			shift
			;;
		-dl|--download-location)
			shift
			if [ $# -gt 0 ]; then
				dl_loc=$1
#				echo "Download Location: $dl_loc"
			else
				echo "No download location given."
				exit 1
			fi
			shift
			;;
		-k|--keywords)
			shift
			if [ $# -gt 0 ]; then
				keywords=$1
			else
				echo "No keywords given."
				exit 1
			fi
#			echo "Keywords: $keywords"
			shift
			;;
		-mt|--max-time)
			shift
			if [ $# -gt 0 ]; then
				max_time=$1
#				echo "Max Time: $max_time"
			else
				echo "No max time given."
				exit 1
			fi
			shift
			;;
		-cln|--clean-files)
			cln=1
			shift
			;;
		--verbose)
			shift
			if [ $# -gt 0 ]; then
				verbosity_lvl=$1
				shift
			else
				verbosity_lvl=1
			fi
#			echo "Verbosity Level: $verbosity_lvl"
			;;
		-v)
			verbosity_lvl=1
			shift
			;;
		-vv)
			verbosity_lvl=2
			shift
			;;
		-vvv)
			verbosity_lvl=3
			shift
			;;
		*)
			break
			;;
	esac
done
#------------------------------------------------------------------#






#==================================================================#
# Give the download location a default if none are provided
#------------------------------------------------------------------#
dl_loc=$( DefaultDownloadLocation $dl_loc )
#------------------------------------------------------------------#
#==================================================================#
# Remove all index files created during gather/install
#------------------------------------------------------------------#
CleanAll()
{
	Verbose 1 $verbosity_lvl "Removing temporary index files..."
	rm -f $menu_file $files_file $all_file 
}
#------------------------------------------------------------------#
#==================================================================#
# Select an index file and collate a menu
#
# Arguments:
#   $1             ---> source address/URL
#   $2 | $menu_sel ---> menu switch
#                       0 : Menu of Files
#                       1 : Menu of Options
#------------------------------------------------------------------#
GetMenu()
{
	declare -i menu_sel=$2
	if [ $menu_sel -eq 0 ]; then
		local grep_regex=$files_grep_regex
		local sed_regex=$files_sed_regex
		local file_sel=$files_file
	else
		local grep_regex=$folders_grep_regex
		local sed_regex=$folders_sed_regex
		local file_sel=$menu_file
	fi
	FreshFile $file_sel
	wget --no-remove-listing --no-parent -q $1 -O - \
		| grep -Po "$grep_regex" \
		| sed -e "$sed_regex" \
		| tac > $file_sel
	local num_options=$(cat $file_sel | wc -l)
	echo $num_options
}
#------------------------------------------------------------------#
#==================================================================#
# Aquire the menu of files directly
#------------------------------------------------------------------#
GetFilesMenu()
{
	wget --no-remove-listing --no-parent --no-http-keep-alive \
		$source -rq -O $all_file
	cat $all_file \
		| grep "$files_grep_regex" \
		| sed -e "$files_sed_regex" > $files_file
	local num_options=`cat $files_file | wc -l`
	echo $num_options
}
#------------------------------------------------------------------#
#==================================================================#
# Uppercase the first letter of each word
#------------------------------------------------------------------#
TitleCase()
{
	cat -n $1 | sed -e "s/.*/\L&/" -e "s/[a-z]*/\u&/g"
}
#------------------------------------------------------------------#
#==================================================================#
# Display the menu to the terminal
#
# Arguments:
#   $1             ---> menu title
#   $2 | $menu_sel ---> menu switch
#                       0 : Menu of Files
#                       1 : Menu of Options
#------------------------------------------------------------------#
DisplayMenu()
{
	declare -i menu_sel=$2
	echo "======================================================="
	echo "Available Options for $1"
	echo "-------------------------------------------------------"
	echo "Source: $source"
	echo "======================================================="
	echo "     0  quit"
	if [ $menu_sel -eq 0 ]; then
		TitleCase $menu_file
	else
		cat -n $files_file
	fi
	echo "======================================================="
	echo "Please, Select A Number: "
}
#------------------------------------------------------------------#
#==================================================================#
# Aquire a file from the web
#
# Arguments:
#   $1 | $src      ---> source address/URL
#   $2 | $file_sel ---> the name of the file once downloaded
#------------------------------------------------------------------#
Download()
{
	local curr=`pwd`
	local src=$1
	local file_sel=$2
	cd $dl_loc
	if [ ! -f $file_sel ]; then
		Verbose 1 $verbosity_lvl "Downloading..."
		if [ $verbosity_lvl -gt 2 ]; then
			q="--progress=bar"
		else
			q="-q"
		fi
		if [ ! -z "$ext" ] && [ -z "$file_sel" ]; then
			accept=$ext
		else
			accept=$file_sel
		fi
		if [ -z "$install_file_pattern" ] || [ -z "$full_url_pattern" ]; then
			out=""
		else
			out="-O $file_sel"
		fi
		
		dl_exp="wget -nd --no-parent -rA $accept $out $q $src"
		Verbose 3 $verbosity_lvl "Expression: $dl_exp"
		eval "$dl_exp"
	else
		Verbose 1 $verbosity_lvl "$file_sel has already been downloaded."
	fi
	cd $curr
}
#------------------------------------------------------------------#
#==================================================================#
# Construct a link based on keywords and a pattern
#
# Arguments:
#   $1 | $line_num ---> line number in index-file containing the
#                       filename of the archive to be downloaded
#   $2 | $file_sel ---> the name of the index-file
#------------------------------------------------------------------#
ConstructLink()
{
	line_num=$1
	file_sel=$2
	local option=$( sed "${line_num}q;d" $file_sel | awk '{print $1;}' )
	if [ ! -z "$install_file_pattern" ] && [ ! -z "$full_url_pattern" ]; then
		install_file=`eval "echo "$install_file_pattern`
		full_url=`eval "echo "$full_url_pattern`
	else
		install_file=$(head -n $line_num $files_file)
		full_url=$source
	fi
	echo $option
}
#------------------------------------------------------------------#
#==================================================================#
# Build the main menu from the top-most level by default
#
# Arguments:
#   $1 | $title  ---> The Menu's title (for user's purposes only)
#   $2 | $loc_dl ---> The base address/URL of the archive to be 
#                     downloaded
#
#                     Both $title and $loc_dl will be built up
#                     dynamically if options present more options
#------------------------------------------------------------------#
RunMainMenu()
{
	declare -i switch=1
	local loc_dl=$2
	local title=$1
	local num_options=$( GetMenu $loc_dl $switch )
	local file_sel=""
	if [ $switch -eq 0 ] || [ $( GetMenu $loc_dl 0 ) -gt 0 ];then
		num_options=$( GetMenu $loc_dl 0 )
		switch=0
	fi
	local x=0
	while [ $x -eq 0 ]; do
		clear
		if [ $switch -gt 0 ];then
			DisplayMenu $title 0
			file_sel=$menu_file
		else
			DisplayMenu $title 1
			file_sel=$files_file
		fi
		read sel
		if [ $sel -gt $num_options ] || [ $sel -lt 0 ]; then
			echo "Select a valid option [ 1-$num_options ] or [ 0 ] to quit."
			sleep 3
		elif [ $sel -eq 0 ]; then
			echo "Goodbye!"
			x=1
		elif [ $sel -gt 0 ] && [ $sel -le $num_options ]; then
#			selection=$( sed "${sel}q;d" $file_sel | awk '{print $1;}' )
			selection=$( ConstructLink $sel $file_sel )
			if [ $switch -gt 0 ]; then
				RunMainMenu $title-${selection[@]^} "$loc_dl$selection/"
			else
				Download $loc_dl$selection $selection
			fi
			x=1
		fi
		clear
	done
}
#------------------------------------------------------------------#
#==================================================================#
# Build the files menu directly
#
# Argument:
#   $1 | $title  ---> The Menu's title (for user's purposes only)
#------------------------------------------------------------------#
RunFilesMenu()
{
	local title=$1
	local num_options=`cat $files_file | wc -l`
	if [ $num_options -eq 0 ]; then
		num_options=$( GetFilesMenu )
	fi
	local x=0
	while [ $x -eq 0 ]; do
		clear
		DisplayMenu $title 1
		read sel
		if [ $sel -gt $num_options ] || [ $sel -lt 0 ]; then
			echo "Select a valid option [ 1-$num_options ] or [ 0 ] to quit."
			sleep 3
		elif [ $sel -eq 0 ]; then
			echo "Goodbye!"
			x=1
		elif [ $sel -gt 0 ] && [ $sel -le $num_options ]; then
			ConstructLink $sel $files_file
			Verbose 2 $verbosity_lvl "$install_file at $full_url"
			Download $full_url $install_file
			x=1
		fi
		clear
	done
}
#------------------------------------------------------------------#
#==================================================================#
# Attempt to obtain an archive directly through the command line
# using enough keywords to uniquely identify the archive at the 
# source address/URL
#
# Argument:
#   $1 ---> Rebuilding the index switch
#           0 : Do not rebuild the index
#           1 : Rebuild the index
#------------------------------------------------------------------#
GetDirect()
{
	if [ $1 -eq 1 ]; then
		FreshFile $all_file
		FreshFile $files_file
		Verbose 1 $verbosity_lvl "Building index of available versions..."
		if [ ! -z $ext ]; then
			wget --no-remove-listing --no-parent $source -rqA $ext -O $all_file
		else
			wget --no-remove-listing --no-parent $source -rq -O $all_file
		fi
	fi
	cat $all_file \
		| grep -Po $files_grep_regex \
		| sed -e $files_sed_regex > $files_file
	if [ ! -z "$keywords" ]; then	
		IFS="$delim" read -ra ADDR <<< $keywords
		for i in ${ADDR[@]}; do
			sed -i -n "/$i/p" $files_file
		done
	fi
	if [ $(wc -l < $files_file) -eq 1 ]; then
		ConstructLink 1 $files_file
		Download $full_url $install_file
	else
		Verbose 1 $verbosity_lvl "Building Menu..."
		if [ -z "$install_file_pattern" ] || [ -z "$full_url_pattern" ]; then
			RunMainMenu $name $source
		else
			RunFilesMenu $name
		fi
	fi
}
#------------------------------------------------------------------#





#==================================================================#
# Main Functionality
#------------------------------------------------------------------#
Verbose 3 $verbosity_lvl "Source: $source"
Verbose 3 $verbosity_lvl "Name: $name"
Verbose 3 $verbosity_lvl "File Pattern: $files_grep_regex"
Verbose 3 $verbosity_lvl "Menu Pattern: $files_sed_regex"
Verbose 3 $verbosity_lvl "Full URL Pattern: $full_url_pattern"
Verbose 3 $verbosity_lvl "Install File Pattern: $install_file_pattern"
Verbose 3 $verbosity_lvl "Extension: $ext"
Verbose 3 $verbosity_lvl "Keywords: $keywords"
Verbose 3 $verbosity_lvl "Deliminator: $delim"
Verbose 3 $verbosity_lvl "Download Location: $dl_loc"
Verbose 3 $verbosity_lvl "Verbosity Level: $verbosity_lvl"
if [ ! -z "$source" ]; then
	if [ ! -z "$keywords"  ]; then
		Verbose 1 $verbosity_lvl "Attempting to obtain $name directly using [ $keywords ]..."
		if [ -f $all_file ]; then
			diff=$( TimeSinceLastModify $all_files )
			if [ $diff -gt $max_time ]; then
				GetDirect 1
			else
				GetDirect 0
			fi
		else
			GetDirect 1
		fi
	else
		if [ -z "$install_file_pattern" ] || [ -z "$full_url_pattern" ]; then
			RunMainMenu $name $source
		else
			RunFilesMenu $name
		fi
	fi
else
	# error
	echo "You need a source URL/ Address!"
fi

if [ $cln -ne 0 ]; then
	CleanAll
fi
#------------------------------------------------------------------#
