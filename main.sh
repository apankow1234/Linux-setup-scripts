#!/bin/bash
#==================================================================#
# Fresh Install
# Getting Up To Speed, In A Single Script, On Any Machine
# 
# By: Andrew Pankow
# 
# 
# 
# 
#------------------------------------------------------------------#
#==================================================================#
# Initialization of key variables
#------------------------------------------------------------------#
source includes/basic_func.sh

global_verbosity=3 # Valid options: 0, 1, 2, or 3
# verbosity is:
# 0 - show nothing during progress
# 1 - show a little; let me know what stage I'm on
# 2 - show more; give me that and some more
# 3 - give me as much as possible

# Recommended versions of software are:
eclipse_recommendation="cpp oxygen 2 rc3"
vulkan_sdk_recommendation="1.0.65.0"
#------------------------------------------------------------------#





#==================================================================#
# Main Interface
#------------------------------------------------------------------#
while [ $# -gt 0 ]; do
	case "$1" in
		-nr|--no-recommend)
			eclipse_recommendation=""
			vulkan_sdk_recommendation=""
#			echo "Using Recommended Versions: No"
			shift
			;;
		--verbose)
			shift
			if [ $# -gt 0 ]; then
				global_verbosity=$1
				shift
			else
				global_verbosity=1 # same as calling `-v`
			fi
#			echo "Verbosity Level: $verbosity_lvl"
			;;
		-v)
			global_verbosity=1
			shift
			;;
		-vv)
			global_verbosity=2
			shift
			;;
		-vvv)
			global_verbosity=3
			shift
			;;
		*)
			break
			;;
	esac
done
#------------------------------------------------------------------#







#==================================================================#
# Drivers & Required But Not-Directly Used Pieces
#------------------------------------------------------------------#
clear
if [ -f /etc/redhat-release ]; then # default for RedHat flavors
	echo "RedHat"
	yum update -y
elif [ -f /etc/debian_version ]; then # default for Debian flavors
	echo "Debian"
	# Eclipse and other software runs on Java
	add-apt-repository ppa:webupd8team/java -y
	apt update -y
	apt install -fy default-jdk openjdk-7-jdk \
		oracle-java8-installer \
		oracle-java8-set-default
	apt install -f
fi
#------------------------------------------------------------------#





#==================================================================#
# Directly Used Pieces - More Control Required
#------------------------------------------------------------------#
#
# Keywords must remain inside the double quotes and space
# separated. Make sure to use enough keywords to uniquely 
# identify the version else will be presented with a menu.
#
# Example: eclipse_keywords="cpp neon 3" ---> downloads item
# Example: eclipse_keywords="cpp neon"   ---> fetches menu
# Example: eclipse_keywords=""           ---> fetches menu
#
# Applications will only allow one version at a time,
# right now. SDK's, libraries, and the like will be able
# to have more than one version.
#
#------------------------------------------------------------------#
#
# Individual installation verbosities can be overridden with
# a valid number; [ 0-3 ] or set to the global verbosity.
#
# Example: eclipse_verbosity=1
# Example: eclipse_verbosity=$global_verbosity ---> default
# 
#------------------------------------------------------------------#
#==================================================================#
# Eclipse
#------------------------------------------------------------------#
# CAN EDIT THIS PART
eclipse_keywords="$eclipse_recommendation"
eclipse_verbosity=$global_verbosity
#
# DON'T NEED TO EDIT THIS PART
bash get/eclipse/get_eclipse.sh \
	--keywords "$eclipse_keywords" \
	--verbose $eclipse_verbosity
#------------------------------------------------------------------#
#==================================================================#
# Vulkan SDK
#------------------------------------------------------------------#
# CAN EDIT THIS PART
vulkan_sdk_keywords="$vulkan_sdk_recommendation"
vulkan_sdk_verbosity=$global_verbosity
#
# DON'T NEED TO EDIT THIS PART
bash get/vulkan_sdk/get_vulkan_sdk.sh \
	--keywords "$vulkan_sdk_keywords" \
	--verbose $vulkan_sdk_verbosity
#------------------------------------------------------------------#

